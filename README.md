# fcc-product-landing-page

- FreeCodeCamp Responsive Web Design Certification. Project #3: Product Landing Page.
- [Project to be solved.](https://www.freecodecamp.org/learn/responsive-web-design/responsive-web-design-projects/build-a-product-landing-page)
- [Solution to project.](https://jserranodev.gitlab.io/fcc-product-landing-page)
